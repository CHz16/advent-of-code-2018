Day 10: The Stars Align
=======================

https://adventofcode.com/2018/day/10

The implementation of the simulation is the easy part of this puzzle: it's literally just the line `lights = lights.map { $0.moved() }` at the end of the loop. The hard part is: how exactly do you know when a message is being displayed? Do you let it run potentially thousands of steps and check each one? Write a text detection algorithm?

Nah. Since each light is moving linearly, unless they have exactly the same velocity, then either the distance between them will decrease until it hits a minimum and then increase to infinity, or it will just increase to infinity. So, my loop checks the distance between the first two lights in the list, and once they reach a certain threshold of closeness, prints out every grid until the distance between them is greater than that threshold. The assumption was that, to form the message, all of the lights would have to be fairly close to each other, so the only grids we care about are the ones where the lights are fairly close to each other. Then, we can just go through those by hand and find the message by inspection.

The tricky part of this is that we have no idea how close the two lights we've arbitrarily selected will actually be, so we need to figure out the threshold by trial and error. I started with 50, which was too low because it never finished executing, but 75 worked great, printing only twenty-one grids.

For the test input and my puzzle input, the first two lights had velocities different enough that they wouldn't cluster with each other for a whole bunch of turns. If I was unlucky enough to have two lights with the same velocity or something, the fix would've been just to reorder the data.

* Part 1: 139th place (17:48)
* Part 2: 137th place (18:53)
