#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "10.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Parts 1 & 2

struct Light {
    let x, y: Int
    let velocityX, velocityY: Int

    func moved() -> Light {
        return Light(x: x + velocityX, y: y + velocityY, velocityX: velocityX, velocityY: velocityY)
    }

    func distance(from: Light) -> Int {
        return abs(x - from.x) + abs(y - from.y)
    }
}

var lights: [Light] = []
input.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: " ").compactMap { Int($0) }
    lights.append(Light(x: components[0], y: components[1], velocityX: components[2], velocityY: components[3]))
}


let THRESHOLD = 75
var reachedThreshold = false, microseconds = 0
while !(reachedThreshold && lights[0].distance(from: lights[1]) > THRESHOLD) {
    reachedThreshold = reachedThreshold || (lights[0].distance(from: lights[1]) <= THRESHOLD)
    if reachedThreshold {
        let upperLeftX = lights.map { $0.x }.min()!, upperLeftY = lights.map { $0.y }.min()!
        let lowerRightX = lights.map { $0.x }.max()!, lowerRightY = lights.map { $0.y }.max()!
        var grid = Array(repeating: Array(repeating: ".", count: lowerRightX - upperLeftX + 1), count: lowerRightY - upperLeftY + 1)

        for light in lights {
            grid[light.y - upperLeftY][light.x - upperLeftX] = "#"
        }
        print(microseconds)
        for row in grid {
            print(row.reduce("", +))
        }
        print("---")
    }

    microseconds += 1
    lights = lights.map { $0.moved() }
}
