//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "2", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

var twos = 0, threes = 0
input.enumerateLines { (line, stop) in
    var counts: [Character: Int] = [:]
    for c in line {
        let count = counts[c] ?? 0
        counts[c] = count + 1
    }

    if counts.values.contains(2) {
        twos += 1
    }
    if counts.values.contains(3) {
        threes += 1
    }
}
print(twos * threes)


// Part 2

print("--------------")

let ids = input.components(separatedBy: "\n")
let idLength = ids[0].count
x: for x in 0..<(ids.count-2) {
    y: for y in (x+1)..<(ids.count-1) {
        var differenceFound = false
        var xI = ids[x].startIndex, yI = ids[y].startIndex
        while xI != ids[x].endIndex {
            if ids[x][xI] != ids[y][yI] {
                if differenceFound {
                    continue y
                } else {
                    differenceFound = true
                }
            }
            xI = ids[x].index(after: xI)
            yI = ids[y].index(after: yI)
        }
        print(ids[x])
        print(ids[y])
        break x
    }
}
