Day 2: Inventory Management System
==================================

https://adventofcode.com/2018/day/2

For part 1 I just did the straightforward thing and counted the occurrences of each character in each ID. Part 2 I fumbled with for a bit as I tried to remember how exactly you're supposed to do character indexing stuff in Swift and took a big hit on the leaderboard in the process. I did it the extremely inefficient nested loops brute force method of just comparing each string to every other string.

* Part 1: 155th place (4:05)
* Part 2: 480th place (15:39)
