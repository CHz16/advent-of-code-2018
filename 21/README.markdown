Day 21: Chronal Conversion
==========================

https://adventofcode.com/2018/day/21

Oh boy, more messing with assembly! I actually kind of hate working with assembly, a lot, lmao

So the first things I did were run the code for a bit to generate an execution log and then use that to help annotate the assembly in something more readable to myself; that's all in comments in `21.txt`. (All my parser does is store the opcode and first three numeric arguments into a struct for later, so any extra content past the first three numbers is ignored.)

I found that the code does some random math in registers 1 & 3, and the only exit point of the code is in instruction 29 based on an equality comparison in instruction 28 between registers 0 (which is never set by the program) and 3. So, the answer to part 1 is just whatever value register 3 has during the first point at which the code reaches instruction 28.

Part 2 is trickier; eventually, the results of the mathematical operations the code start looping, so the answer is whatever the last distinct value of register 3 is.

To help this along, I added two new operations to the machine, both of which are used in a modified version of the code in `21-2.swift`. The first is a performance boost: I noticed a big chunk of the code was devoted to calculating the integer division of a register by 256, which takes an incredibly long time to do with addition and multiplication instructions only. So, I added an operation `divi`, which divides the register arg0 by immediate value arg1 and puts the result into register arg2. This made it much more feasible to actually run the assembly.

The second is a logging instruction, `logr`, that's run right before the code does the halting comparison and does the tracking of the register values. Whenever a new register 3 value is encountered, it prints it out, so the answer to part 1 is the first printed value and the answer to part 2 is the last value. It also checks to see if we've looped, and if so, it automatically sets register 0 to the value in register 3 so execution will terminate.

The tricky part is the "checks to see if we've looped" part, because it's possible that register 3 may hit the same value multiple times during the cycle! I didn't realize this at first and submitted the wrong value. The problem is that, as I said earlier, it does math on registers 1 & 3, so the process only loops when the **combination of both registers** is one that we've seen before. So the logging instruction also holds on to a second set to keep track of that.

* Part 1: 164th place (31:23)
* Part 2: 70th place (50:58)
