#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "8.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

let stream = input.components(separatedBy: .whitespacesAndNewlines).compactMap { Int($0) }
var children: [[Int]] = []
var metadata: [[Int]] = []

func readNode(streamIndex: Int, nodeIndex: Int) -> (Int, Int) {
    let numberOfChildren = stream[streamIndex]
    let numberOfMetadata = stream[streamIndex + 1]
    children.append([])
    metadata.append([])

    var streamIndex = streamIndex + 2
    var childNodeIndex = nodeIndex + 1
    for _ in 0..<numberOfChildren {
        children[nodeIndex].append(childNodeIndex)
        (streamIndex, childNodeIndex) = readNode(streamIndex: streamIndex, nodeIndex: childNodeIndex)
    }

    for _ in 0..<numberOfMetadata {
        metadata[nodeIndex].append(stream[streamIndex])
        streamIndex += 1
    }

    return (streamIndex, childNodeIndex)
}
(_, _) = readNode(streamIndex: 0, nodeIndex: 0)

print(metadata.reduce(0) { $0 + $1.reduce(0, +) } )


// Part 2

print("--------------")

func nodeSum(_ nodeIndex: Int) -> Int {
    if children[nodeIndex].count == 0 {
        return metadata[nodeIndex].reduce(0, +)
    } else {
        return metadata[nodeIndex].reduce(0) {
            if $1 == 0 || $1 > children[nodeIndex].count {
                return $0
            } else {
                return $0 + nodeSum(children[nodeIndex][$1 - 1])
            }
        }
    }
}
print(nodeSum(0))
