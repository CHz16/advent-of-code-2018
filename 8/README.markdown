Day 8: Memory Maneuver
======================

https://adventofcode.com/2018/day/8

Parsing! Instead of building an actual tree, I just populated a couple of arrays with the pertinent data, which sure made part 1 real easy.

I messed up with the tree generation and lost a little bit of time on part 2, but still picked up a bunch of places somehow.

* Part 1: 803rd place (25:55)
* Part 2: 671st place (34:29)
