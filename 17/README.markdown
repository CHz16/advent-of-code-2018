Day 17: Reservoir Research
==========================

https://adventofcode.com/2018/day/17

So this one is really hard lmao

Some problems that occurred during my coding of this:

1). I started implementing this with a stack, which failed in situations like my `test2.txt`:

```
......+.....#.
..............
.#........#...
.#........#...
.#...###..#...
.#........#...
.#........#...
.#........#...
.##########...
```

The problem was that the water would fall until it hit the clay platform and then spread out, finding outlets on both sides. However, since the basin is enclosed, it turns out that those outlets aren't outlets after all, so the water should keep rising past that platform. My stack version didn't have any way to "inform" the water source that the outlets turned out not to be outlets, so the propagation stopped there:

```
......|.....#.
......|.......
.#....|...#...
.#~~~|||~~#...
.#~~~###~~#...
.#~~~~~~~~#...
.#~~~~~~~~#...
.#~~~~~~~~#...
.##########...
```

My solution was to switch from a stack to recursion, which immediately just worked.

2). I had the logical OR on line 97 reversed, which would cause my code not to cause water to fall from the right side of the scan if it found an outlet on the left side.

3). I didn't read the whole problem carefully enough and ignored this part: "ignore tiles with a `y` coordinate smaller than the smallest `y` coordinate in your scan data," so my answer was off by two and it took me a long time to figure it out lmao

Update: `17-2.swift` has some changes:

1). Changed the `waterFalling` and `waterSpreading` states to `waterFlowing` and `waterStationary`, which are more accurate. I didn't really figure that out until part 2, and I never bothered changing it.

2). Fixed a bug where the initial water source would clobber a clay block if there was one in the first row of scan data right below it. This didn't come up in my puzzle input.

3). Coalesced the left and right spread scans into just one loop.

4). Fixed a bug in the spreading phase of the recursion where it would treat `waterFlowing` blocks to the sides as walls instead of outlets. This is illustrated in `test3.txt`, where there should be no stationary water at all. This also didn't come up in my puzzle input.

* Part 1: 206th place (1:45:38)
* Part 2: 203rd place (1:49:03)
