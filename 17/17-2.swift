#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "17.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

enum State { case empty, clay, waterFlowing, waterStationary }

// Scan for dimensions
var minX = Int.max, maxX = Int.min
var minY = Int.max, maxY = Int.min
input.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: " ")
    let numbers = components[1..<4].compactMap { Int($0) }
    if components[0] == "x" {
        minX = min(minX, numbers[0])
        maxX = max(maxX, numbers[0])
        minY = min(minY, numbers[1])
        maxY = max(maxY, numbers[2])
    } else {
        minY = min(minY, numbers[0])
        maxY = max(maxY, numbers[0])
        minX = min(minX, numbers[1])
        maxX = max(maxX, numbers[2])
    }
}
// Overflow padding past the clay blocks on either side, one column should be enough(?)
minX -= 1
maxX += 1
// One more row in case the initial water source would be directly on top of a clay block. We'll cut this off later.
minY -= 1

// Lay out the grid
var grid = Array(repeating: Array(repeating: State.empty, count: maxX - minX + 1), count: maxY - minY + 1)
input.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: " ")
    let numbers = components[1..<4].compactMap { Int($0) }
    if components[0] == "x" {
        for y in numbers[1]...numbers[2] {
            grid[y - minY][numbers[0] - minX] = .clay
        }
    } else {
        for x in numbers[1]...numbers[2] {
            grid[numbers[0] - minY][x - minX] = .clay
        }
    }
}

// Water simulation
// Returns true if the water source has an outlet, false otherwise
func propagateWater(_ x: Int, _ y: Int) -> Bool {
    grid[y][x] = .waterFlowing

    // The bottom is an automatic outlet
    if y == grid.count - 1 {
        return true
    }

    // If there's nothing under the current point, then create a new falling water source under it
    if grid[y + 1][x] == .empty {
        _ = propagateWater(x, y + 1)
    }

    // If this water source is falling on already falling water, then it's falling on something with an outlet
    if grid[y + 1][x] == .waterFlowing {
        return true
    }

    // Otherwise, spread left and right until we hit something or find something interesting below.
    var foundOutlet = false
    var spreadPoints: [(x: Int, y: Int)] = [(x: x, y: y)]

    let scanDirections = [-1, 1] // Left, right
    for scanDirection in scanDirections {
        var scanX = x + scanDirection
        while true {
            if grid[y][scanX] == .clay || grid[y][scanX] == .waterStationary {
                // If we hit a wall, then stop scanning
                // I think the stationary check here is unnecessary but whatev
                break
            } else if grid[y][scanX] == .waterFlowing {
                // If we hit flowing water, then report that we've found an outlet and stop scanning
                foundOutlet = true
                break
            }

            // Current square is empty: add water to it
            spreadPoints.append((x: scanX, y: y))

            // If there's flowing water below us, then report we've found an outlet and stop scanning
            if grid[y + 1][scanX] == .waterFlowing {
                foundOutlet = true
                break
            }

            // If there's empty space below us, then drop water there. If we find an outlet, then report that and stop scanning
            if grid[y + 1][scanX] == .empty && propagateWater(scanX, y + 1) {
                foundOutlet = true
                break
            }

            // Otherwise, keep scanning
            scanX += scanDirection
        }
    }

    for (x, y) in spreadPoints {
        grid[y][x] = foundOutlet ? .waterFlowing : .waterStationary
    }
    return foundOutlet
}
_ = propagateWater(500 - minX, 0)


for row in grid {
    print(row.reduce("") {
        switch $1 {
        case .empty:
            return $0 + "."
        case .waterFlowing:
            return $0 + "|"
        case .waterStationary:
            return $0 + "~"
        case .clay:
            return $0 + "#"
        }
    })
}

var standingWater = 0, flowingWater = 0
for row in grid.dropFirst() {
    for cell in row {
        if cell == .waterFlowing {
            flowingWater += 1
        } else if cell == .waterStationary {
            standingWater += 1
        }
    }
}
print(standingWater + flowingWater)


// Part 2

print("--------------")
print(standingWater)
