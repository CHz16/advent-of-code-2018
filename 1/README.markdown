Day 1: Chronal Calibration
==========================

https://adventofcode.com/2018/day/1

Extremely simple puzzle about adding a bunch of numbers together and keeping track of the intermediate sums. The only really tricky part is that, in part 2, you might (will) have to repeat the list several times. A lot of times actually, it took the playground a bit to finish execution haha.

I'm probably going to be using the `compactMap` trick on line 11 a whole bunch this year to parse lists of integers from the input data. If we ran `map { Int($0) }` on the input, then the return type would be `[Int?]` because the `Int` constructor will fail on non-numeric values and return `nil`. `compactMap` however will return `[Int]` by unwrapping the optionals and tossing away the `nil`s, which is completely perfect for our purposes here.

* Part 1: 513th place (3:30)
* Part 2: 239th place (8:07)
