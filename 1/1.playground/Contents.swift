//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "1", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

let adjustments = input.components(separatedBy: "\n").compactMap { Int($0) }
print(adjustments.reduce(0, +))


// Part 2

print("--------------")

var currentFrequency = 0
var frequencies = Set<Int>([currentFrequency])
outside: while true {
    for adjustment in adjustments {
        currentFrequency += adjustment
        if frequencies.contains(currentFrequency) {
            print(currentFrequency)
            break outside
        }
        frequencies.insert(currentFrequency)
    }
}
