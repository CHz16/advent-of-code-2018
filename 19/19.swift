#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "test2.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

enum Operation {
    case addRegister, addImmediate, multiplyRegister, multiplyImmediate, bitwiseAndRegister, bitwiseAndImmediate, bitwiseOrRegister, bitwiseOrImmediate, setRegister, setImmediate, greaterThanImmediateRegister, greaterThanRegisterImmediate, greaterThanRegisterRegister, equalImmediateRegister, equalRegisterImmediate, equalRegisterRegister

    init(_ mnemonic: String) {
        if mnemonic == "addr" {
            self = .addRegister
        } else if mnemonic == "addi" {
            self = .addImmediate
        } else if mnemonic == "mulr" {
            self = .multiplyRegister
        } else if mnemonic == "muli" {
            self = .multiplyImmediate
        } else if mnemonic == "banr" {
            self = .bitwiseAndRegister
        } else if mnemonic == "bani" {
            self = .bitwiseAndImmediate
        } else if mnemonic == "borr" {
            self = .bitwiseOrRegister
        } else if mnemonic == "bori" {
            self = .bitwiseOrImmediate
        } else if mnemonic == "setr" {
            self = .setRegister
        } else if mnemonic == "seti" {
            self = .setImmediate
        } else if mnemonic == "gtir" {
            self = .greaterThanImmediateRegister
        } else if mnemonic == "gtri" {
            self = .greaterThanRegisterImmediate
        } else if mnemonic == "gtrr" {
            self = .greaterThanRegisterRegister
        } else if mnemonic == "eqir" {
            self = .equalImmediateRegister
        } else if mnemonic == "eqri" {
            self = .equalRegisterImmediate
        } else { // eqrr
            self = .equalRegisterRegister
        }
    }
}

struct Instruction { let operation: Operation, arguments: [Int] }

func execute(_ instruction: Instruction, on registers: [Int]) -> [Int] {
    let operation = instruction.operation
    let arguments = instruction.arguments

    var registers = registers
    switch operation {
    case .addRegister:
        registers[arguments[2]] = registers[arguments[0]] + registers[arguments[1]]
    case .addImmediate:
        registers[arguments[2]] = registers[arguments[0]] + arguments[1]
    case .multiplyRegister:
        registers[arguments[2]] = registers[arguments[0]] * registers[arguments[1]]
    case .multiplyImmediate:
        registers[arguments[2]] = registers[arguments[0]] * arguments[1]
    case .bitwiseAndRegister:
        registers[arguments[2]] = registers[arguments[0]] & registers[arguments[1]]
    case .bitwiseAndImmediate:
        registers[arguments[2]] = registers[arguments[0]] & arguments[1]
    case .bitwiseOrRegister:
        registers[arguments[2]] = registers[arguments[0]] | registers[arguments[1]]
    case .bitwiseOrImmediate:
        registers[arguments[2]] = registers[arguments[0]] | arguments[1]
    case .setRegister:
        registers[arguments[2]] = registers[arguments[0]]
    case .setImmediate:
        registers[arguments[2]] = arguments[0]
    case .greaterThanImmediateRegister:
        registers[arguments[2]] = (arguments[0] > registers[arguments[1]]) ? 1 : 0
    case .greaterThanRegisterImmediate:
        registers[arguments[2]] = (registers[arguments[0]] > arguments[1]) ? 1 : 0
    case .greaterThanRegisterRegister:
        registers[arguments[2]] = (registers[arguments[0]] > registers[arguments[1]]) ? 1 : 0
    case .equalImmediateRegister:
        registers[arguments[2]] = (arguments[0] == registers[arguments[1]]) ? 1 : 0
    case .equalRegisterImmediate:
        registers[arguments[2]] = (registers[arguments[0]] == arguments[1]) ? 1 : 0
    case .equalRegisterRegister:
        registers[arguments[2]] = (registers[arguments[0]] == registers[arguments[1]]) ? 1 : 0
    }
    return registers
}


var ipRegister = 0
var instructions: [Instruction] = []
input.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: " ")
    if components[0] == "#ip" {
        ipRegister = Int(components[1])!
    } else {
        let arguments = Array(components[1..<4].compactMap { Int($0) })
        instructions.append(Instruction(operation: Operation(components[0]), arguments: arguments))
    }
}

var registers = [0, 0, 0, 0, 0, 0]
var ip = 0
while ip >= 0 && ip < instructions.count {
    registers[ipRegister] = ip
    registers = execute(instructions[ip], on: registers)
    ip = registers[ipRegister]
    ip += 1
}
print(registers)
