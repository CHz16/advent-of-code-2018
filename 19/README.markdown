Day 19: Go With The Flow
========================

https://adventofcode.com/2018/day/19

So the first part is slightly updating the VM from day 16 to have a modifiable instruction pointer. That took me a couple of tries to get right (because I didn't fully understand the problem before applying fingers to keyboard), and also I had to write a bunch more code to convert opcode mnemonics to instructions.

Then part 2 is to run the same code as part 1 but with a change in the initial values of the registers that cause it to never finish executing, so you need to figure out what the code is actually doing and then optimize it or do the calculation yourself.

I printed out the registers for the first bit of execution and followed the code along with it, and figured out that certain lines of it were used to load a specific value into register 1. The memory change for part 2 makes the code take a branch that loads a significantly larger number into register 1 instead.

Then, I made a copy of the code in `test2.txt` and changed all those instructions but one into noops (`addi 0 0 0`), and then changed the last one to load that first specific value directly into register 1. That way, I could load my own values into the register very easily and then figure out what calculation it was doing by observation. I figured it out really quickly after that and then just punched the part 2 value into a website that did the calculation for me (I am intentionally not saying what that calculation was). This was apparently good enough to vault me into 20th place lmao what the heck

* Part 1: 215th place (17:36)
* Part 2: 20th place (33:35)
