Day 22: Mode Maze
=================

https://adventofcode.com/2018/day/2

Oh boy, time for an Actual Pathfinding Algorithm! Part 1 is just generating a grid with three different terrain types, and then part 2 is finding a path across that grid according to a couple of rules. So we just dropped some A* for part 2, no big deal.

Except for the priority queue part of A*, which I first tried to ignore by just putting everything into an Array and sorting that array every iteration. That ran like garbage. So instead I switched to inserting sort for each new element in the array. Which also ran like garbage. So instead I switched to a singly-linked list to skip the penalty for inserting into the middle of an Array all the time. Which also ran like garbage, but only just two minutes of garbage so that was fine.

* Part 1: 177th place (19:16)
* Part 2: 235th place (1:42:34)
