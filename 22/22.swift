#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "22.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

let components = input.components(separatedBy: .whitespacesAndNewlines).compactMap { Int($0) }
let depth = components[0]
let target = (x: components[1], y: components[2])

var grid = Array(repeating: Array(repeating: 0, count: target.x + 200), count: target.y + 200) // padding for part 2 pathfinding lmao
for y in 0..<grid.count {
    for x in 0..<grid[0].count {
        let geologicIndex: Int
        if x == target.x && y == target.y {
            geologicIndex = 0
        } else if x == 0 {
            geologicIndex = y * 48271
        } else if y == 0 {
            geologicIndex = x * 16807
        } else {
            geologicIndex = grid[y - 1][x] * grid[y][x - 1]
        }
        grid[y][x] = (geologicIndex + depth) % 20183
    }
}

var risk = 0
for y in 0...target.y {
    for x in 0...target.x {
        risk += grid[y][x] % 3
    }
}
print(risk)


// Part 2

print("--------------")

enum Tool: Int, CaseIterable { case torch, climbingGear, neither }
struct SearchState {
    let x: Int, y: Int, tool: Tool
    let distance: Int, heuristic: Int

    init(x: Int, y: Int, tool: Tool, distance: Int) {
        self.x = x
        self.y = y
        self.tool = tool
        self.distance = distance

        let heuristic = distance + abs(y - target.y) + abs(x - target.x) + (tool != .torch ? 7 : 0)
        self.heuristic = heuristic
    }
}

func canUse(tool: Tool, onTerrain erosionLevel: Int) -> Bool {
    if erosionLevel % 3 == 0 {
        // Rocky
        return (tool == .climbingGear || tool == .torch)
    } else if erosionLevel % 3 == 1 {
        // Wet
        return (tool == .climbingGear || tool == .neither)
    } else {
        // Narrow
        return (tool == .torch || tool == .neither)
    }
}

class Node {
    let searchState: SearchState
    var next: Node?

    init(_ searchState: SearchState) {
        self.searchState = searchState
    }
}


var distances = Array(repeating: Array(repeating: Array(repeating: Int.max, count: Tool.allCases.count), count: grid[0].count), count: grid.count)
var queueHead: Node? = Node(SearchState(x: 0, y: 0, tool: .torch, distance: 0))
while let node = queueHead {
    let searchState = node.searchState
    queueHead = node.next

    // If we've already gotten here faster, then quit
    if distances[searchState.y][searchState.x][searchState.tool.rawValue] <= searchState.distance {
        continue
    }
    distances[searchState.y][searchState.x][searchState.tool.rawValue] = searchState.distance

    // Check for goal
    if searchState.x == target.x && searchState.y == target.y && searchState.tool == .torch {
        print(distances[searchState.y][searchState.x][searchState.tool.rawValue])
        break
    }

    // Check if we can move to adjacent squares
    var statesToAdd: [SearchState] = []
    for (newX, newY) in [(x: searchState.x - 1, y: searchState.y), (x: searchState.x + 1, y: searchState.y), (x: searchState.x, y: searchState.y - 1), (x: searchState.x, y: searchState.y + 1)] {
        if newX < 0 || newY < 0 {
            continue
        }

        if canUse(tool: searchState.tool, onTerrain: grid[newY][newX]) {
            statesToAdd.append(SearchState(x: newX, y: newY, tool: searchState.tool, distance: searchState.distance + 1))
        }
    }

    // Check if we can swap tools
    for newTool in Tool.allCases {
        if newTool == searchState.tool {
            continue
        }
        if canUse(tool: newTool, onTerrain: grid[searchState.y][searchState.x]) {
            statesToAdd.append(SearchState(x: searchState.x, y: searchState.y, tool: newTool, distance: searchState.distance + 7))
        }
    }

    // Insert each new state into the queue
    for newState in statesToAdd {
        var newNode = Node(newState)
        if queueHead == nil {
            queueHead = newNode
            continue
        } else if queueHead!.searchState.heuristic >= newState.heuristic {
            newNode.next = queueHead
            queueHead = newNode
            continue
        }

        var inserted = false
        var prev = queueHead!
        while let next = prev.next {
            if next.searchState.heuristic >= newState.heuristic {
                prev.next = newNode
                newNode.next = next
                inserted = true
                break
            }
            prev = next
        }
        if !inserted {
            prev.next = newNode
        }
    }
}
