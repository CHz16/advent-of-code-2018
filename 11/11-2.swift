#!/usr/bin/env xcrun swift

import Cocoa

let input = 4842

let gridSize = 300


// Part 1

var grid = Array(repeating: Array(repeating: 0, count: gridSize + 1), count: gridSize + 1)
var summedAreas = Array(repeating: Array(repeating: 0, count: gridSize + 1), count: gridSize + 1)
for y in 1..<gridSize {
    for x in 1..<gridSize {
        let rackID = x + 10
        let power = ((((rackID * y + input) * rackID) % 1000) / 100) - 5
        grid[y][x] = power
        summedAreas[y][x] = power + summedAreas[y][x - 1] + summedAreas[y - 1][x] - summedAreas[y - 1][x - 1]
    }
}

var maxPower = Int.min
for y in 3..<gridSize {
    for x in 3..<gridSize {
        let subgridPower = summedAreas[y][x] + summedAreas[y - 3][x - 3] - summedAreas[y - 3][x] - summedAreas[y][x - 3]
        if subgridPower > maxPower {
            maxPower = subgridPower
            print(maxPower, x - 2, y - 2)
        }
    }
}


// Part 2

print("--------------")

maxPower = Int.min
for y in 1..<gridSize {
    for x in 1..<gridSize {
        for size in 1...min(x, y) {
            let power = summedAreas[y][x] + summedAreas[y - size][x - size] - summedAreas[y - size][x] - summedAreas[y][x - size]
            if power > maxPower {
                maxPower = power
                print(maxPower, x - size + 1, y - size + 1, size)
            }
        }

    }
}
