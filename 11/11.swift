#!/usr/bin/env xcrun swift

import Cocoa

let input = 4842

let gridSize = 300


// Part 1

var grid = Array(repeating: Array(repeating: 0, count: gridSize), count: gridSize)
var maxPower = Int.min
for y in 0..<gridSize {
    for x in 0..<gridSize {
        let rackID = (x + 1) + 10
        grid[y][x] = ((((rackID * (y + 1) + input) * rackID) % 1000) / 100) - 5
        if x >= 2 && y >= 2 {
            let power = grid[y][x] + grid[y - 1][x] + grid[y - 2][x] + grid[y][x - 1] + grid[y - 1][x - 1] + grid[y - 2][x - 1] + grid[y][x - 2] + grid[y - 1][x - 2] + grid[y - 2][x - 2]
            if power > maxPower {
                maxPower = power
                print(maxPower, x + 1 - 2, y + 1 - 2)
            }
        }
    }
}


// Part 2

print("--------------")

maxPower = Int.min
outer: for y in 0..<gridSize {
    for x in 0..<gridSize {
        var power = 0
        for size in 0...min(x, y) {
            for subgridX in (x-size)...x {
                power += grid[y - size][subgridX]
            }
            if size != 0 {
                for subgridY in (y-size+1)...y {
                    power += grid[subgridY][x - size]
                }
            }

            if power > maxPower {
                maxPower = power
                print(maxPower, x - size + 1, y - size + 1, size + 1)
            }
        }

    }
}

