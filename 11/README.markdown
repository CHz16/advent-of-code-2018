Day 11: Chronal Charge
======================

https://adventofcode.com/2018/day/11

I just brute forced this lmao, which has terrible perf on part 2 (which may be why I dropped so many places). I'm sure this can be done much faster by saving some partial sums or something.

There is a slight optimization in part 2 that calculates all the squares from a point in sequence without having to resum elements by keeping the last square and just adding an extra row and column to it I guess.

The gridSize parameter was just there for testing the part 2 code with smaller than 300x300 grids

UPDATE: I just learned about [summed-area tables](https://en.wikipedia.org/wiki/Summed-area_table) and implemented them in `11-2.swift` and boy howdy they work real nice huh

* Part 1: 251st place (10:07)
* Part 2: 586th place (36:01)
