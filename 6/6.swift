#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "6.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

struct Cell {
    let row, col: Int

    func adjacents() -> [Cell] {
        return [
            Cell(row: row - 1, col: col),
            Cell(row: row + 1, col: col),
            Cell(row: row, col: col - 1),
            Cell(row: row, col: col + 1)
        ]
    }
}

// Read in the input and find the bounds of the grid
var seeds: [Cell] = []
var minCol = Int.max, maxCol = Int.min, minRow = Int.max, maxRow = Int.min
input.enumerateLines { (line, stop) in
    let coords = line.components(separatedBy: " ").compactMap { Int($0) }
    seeds.append(Cell(row: coords[1], col: coords[0]))
    minCol = min(minCol, coords[0])
    maxCol = max(maxCol, coords[0])
    minRow = min(minRow, coords[1])
    maxRow = max(maxRow, coords[1])
}
seeds = seeds.map { Cell(row: $0.row - minRow, col: $0.col - minCol) }

// Create the grid and BFS frontier
var grid = Array(repeating: Array(repeating: (id: "#", dirty: false), count: maxCol - minCol + 1), count: maxRow - minRow + 1)
var frontier: [Cell] = []
for (index, seed) in seeds.enumerated() {
    grid[seed.row][seed.col] = (id: String(index), dirty: false)
    frontier.append(contentsOf: seed.adjacents())
}


// Just a utility func
func isValidCell(_ cell: Cell) -> Bool {
    return cell.row >= 0 && cell.row < grid.count && cell.col >= 0 && cell.col < grid[0].count
}

// Repeat until we haven't reached any new cells on a pass
var dirty: [Cell]
repeat {
    dirty = []

    // Check every cell on the frontier
    while !frontier.isEmpty {
        // If it's out of bounds or visited already then pass
        let cell = frontier.removeFirst()
        if !isValidCell(cell) || grid[cell.row][cell.col].id != "#" {
            continue
        }

        // Check all the adjacent cells that we haven't visited on this pass
        let sources = Set(cell.adjacents().filter(isValidCell).map { grid[$0.row][$0.col] }.filter { $0.id != "#" && !$0.dirty }.map { $0.id })
        if sources.count > 1 {
            // If there are two or more sources, then "."
            grid[cell.row][cell.col] = (id: ".", dirty: true)
        } else {
            // Otherwise mark this cell with the one source and flag it as dirty
            grid[cell.row][cell.col] = (id: sources.first!, dirty: true)
        }
        dirty.append(cell)
    }

    // Clean all the dirty cells for the next pass
    for cell in dirty {
        grid[cell.row][cell.col] = (id: grid[cell.row][cell.col].id, dirty: false)
        frontier.append(contentsOf: cell.adjacents())
    }
} while !dirty.isEmpty


// Count up every cell
var counts: [String: Int] = [:]
for row in grid {
    for cell in row {
        counts[cell.id] = (counts[cell.id] ?? 0) + 1
    }
}

// Clear any region that touches an edge of the grid, because those will go forever
counts["."] = nil
for cell in grid[0] {
    if counts[cell.id] != nil {
        counts[cell.id] = nil
    }
}
for cell in grid.last! {
    if counts[cell.id] != nil {
        counts[cell.id] = nil
    }
}
for i in 0..<grid.count {
    let leftID = grid[i][0].id
    if counts[leftID] != nil {
        counts[leftID] = nil
    }
    let rightID = grid[i].last!.id
    if counts[rightID] != nil {
        counts[rightID] = nil
    }
}

// ok answer
print(counts)
print(counts.values.max()!)


// Part 2

print("--------------")

let padding = 100
var safe = 0
for row in -padding..<(grid.count+padding) {
    for col in -padding..<(grid[0].count+padding) {
        let score = seeds.reduce(0) { $0 + abs(row - $1.row) + abs(col - $1.col) }
        if score < 10000 {
            safe += 1
        }
    }
}
print(safe)
