Day 6: Chronal Coordinates
==========================

https://adventofcode.com/2018/day/6

For the first part I decided it would be an amazing idea to go full breadth-first search, which took me an hour. The grid is not super large and there are only fifty coordinates so it probably would've been faster and easier to just iterate through the cells and check the distances to each source. WHOOPS

I'm not 100% sure it's correct, but I'm relatively convinced this is correct: we can constrain the grid to tightly fit around every source without any padding rows or columns, and any region that touches the edge of the grid will be infinite.

For part 2, I just went through every cell in the grid and calculated the distances lmao. I added padding rows/cols to the iteration in case it was possible for cells outside the tight threshold to have a low enough score, but for my input at least it didn't matter.

* Part 1: 1032nd place (1:02:23)
* Part 2: 889th place (1:22:37)
