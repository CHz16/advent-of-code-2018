Day 18: Settlers of The North Pole
==================================

https://adventofcode.com/2018/day/18

Another simple cellular automata puzzle, 2D this time though. Part 2 is slightly trickier because the grid doesn't just stabilize on one configuration forever; instead, it forms a loop of a certain cycle length, so you need to detect when it forms a cycle and then you can use that to find what the state will be in the extreme future. I tried to do part 2 by hand, but messed up the calculation somewhere (probably an off-by-one error), so I lost a smidge of time there coding it after.

* Part 1: 255th place (18:30)
* Part 2: 295th place (35:37)
