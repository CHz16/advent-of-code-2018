#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "18.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

enum Acre: CustomStringConvertible {
    case open, trees, lumberyard

    var description: String {
        switch self {
        case .open:
            return "."
        case .trees:
            return "|"
        case .lumberyard:
            return "#"
        }
    }
}

func stepGrid(_ grid: [[Acre]]) -> [[Acre]] {
    var newGrid = grid
    for y in 0..<grid.count {
        for x in 0..<grid[0].count {
            let adjacents = [(x: x - 1, y: y - 1), (x: x, y: y - 1), (x: x + 1, y: y - 1), (x: x - 1, y: y), (x: x + 1, y: y), (x: x - 1, y: y + 1), (x: x, y: y + 1), (x: x + 1, y: y + 1)].filter { $0.x >= 0 && $0.x < grid[0].count && $0.y >= 0 && $0.y < grid.count }
            var adjacentCounts = [Acre.open: 0, Acre.trees: 0, Acre.lumberyard: 0]
            for (adjX, adjY) in adjacents {
                adjacentCounts[grid[adjY][adjX]]! += 1
            }

            if grid[y][x] == .open && adjacentCounts[.trees]! >= 3 {
                newGrid[y][x] = .trees
            } else if grid[y][x] == .trees && adjacentCounts[.lumberyard]! >= 3 {
                newGrid[y][x] = .lumberyard
            } else if grid[y][x] == .lumberyard && (adjacentCounts[.lumberyard]! == 0 || adjacentCounts[.trees]! == 0) {
                newGrid[y][x] = .open
            }
        }
    }
    return newGrid
}

func value(of grid: [[Acre]]) -> Int {
    var trees = 0, lumberyards = 0
    for row in grid {
        for acre in row {
            if acre == .trees {
                trees += 1
            } else if acre == .lumberyard {
                lumberyards += 1
            }
        }
    }
    return trees * lumberyards
}


var initialGrid: [[Acre]] = []
input.enumerateLines { (line, stop) in
    var row: [Acre] = []
    for char in line {
        if char == "." {
            row.append(.open)
        } else if char == "|" {
            row.append(.trees)
        } else {
            row.append(.lumberyard)
        }
    }
    initialGrid.append(row)
}

var grid = initialGrid
for _ in 0..<10 {
    grid = stepGrid(grid)
}

for row in grid {
    print(row.map { $0.description }.reduce("", +))
}
print(value(of: grid))


// Part 2

print("--------------")

var seenStates: [[[Acre]]] = []
var seenHashes: [Int: [Int]] = [:]
var loopStart = 0, loopEnd = 0
grid = initialGrid
outer: for i in 0..<10000 {
    let hash = value(of: grid)
    if let indexes = seenHashes[hash] {
        for pastIndex in indexes {
            if seenStates[pastIndex] == grid {
                loopStart = pastIndex
                loopEnd = i - 1
                break outer
            }
        }
    }

    seenStates.append(grid)
    if seenHashes[hash] == nil {
        seenHashes[hash] = []
    }
    seenHashes[hash]!.append(i)

    grid = stepGrid(grid)
}
print(loopStart, "->", loopEnd)
print(value(of: seenStates[loopStart + (1000000000 - loopStart) % (loopEnd - loopStart + 1)]))
