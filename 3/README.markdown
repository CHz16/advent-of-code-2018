Day 3: No Matter How You Slice It
=================================

https://adventofcode.com/2018/day/3

This ended up being way overengineered lmao oops. Get used to that! We just plot every rectangle into a 1000x1000ish grid one cell at a time, no cleverness at all.

Three grid states are necessary because for some reason I wanted to count the number of overlapping cells with one pass through the claim list without having to iterate over the whole grid after, so those are needed to avoid double counting cells. Probably would've been faster to code if I had just iterated over the whole grid after.

* Part 1: 835th place (18:18)
* Part 2: 665th place (23:03)
