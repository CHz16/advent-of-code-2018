#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "3.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Parts 1 & 2

struct Claim {
    let left, top: Int
    let width, height: Int
}


var claims: [Claim] = []
var gridRight = 0, gridBottom = 0
input.enumerateLines { (line, stop) in
    let claimInfo = line.components(separatedBy: " ").compactMap { Int($0) }
    let claim = Claim(left: claimInfo[0], top: claimInfo[1], width: claimInfo[2], height: claimInfo[3])
    claims.append(claim)

    gridRight = max(gridRight, claim.left + claim.width - 1)
    gridBottom = max(gridBottom, claim.top + claim.height - 1)
}


enum CellState { case Unclaimed, Claimed(Int), Overlapped }
var grid: [[CellState]] = []
for _ in 0..<(gridBottom + 1) {
    grid.append(Array(repeating: .Unclaimed, count: gridRight + 1))
}

var overlapped = Array(repeating: false, count: claims.count)
var overlapCount = 0
for (id, claim) in claims.enumerated() {
    for row in (claim.top)..<(claim.top + claim.height) {
        for col in (claim.left)..<(claim.left + claim.width) {
            switch grid[row][col] {
            case .Unclaimed:
                grid[row][col] = .Claimed(id)
            case let .Claimed(otherId):
                grid[row][col] = .Overlapped
                overlapped[id] = true
                overlapped[otherId] = true
                overlapCount += 1
            case .Overlapped:
                overlapped[id] = true
            }
        }
    }
}

print(overlapCount)
print("--------------")
print(overlapped.firstIndex(of: false)! + 1)
