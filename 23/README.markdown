Day 23: Experimental Emergency Teleportation
============================================

https://adventofcode.com/2018/day/23

I gave up on this problem and had to steal [an algorithm from the subreddit](https://www.reddit.com/r/adventofcode/comments/a8sqov/help_day_23_part_2_any_provably_correct_fast/ece5hjw/) for the second part lmao.

The first step was to come up with a function that checks if a nanobot's signal area intersects a cube; I came up with something that I think finds the point on the cube closest to the nanobot's center and then checks if that point is within the radius, but I'm not entirely sure it's correct? The site accepted my answer so that's probably fine.

With that, the algorithm is to have a priority queue of cubes, sorted by highest number of nanobots that intersect it, with the tiebreaker being the cube's distance to the origin, and another tiebreaker on that being smaller cubes first (because they might be closer to the answer). The cube starts with a cube that spans every single nanobot's center, with the dimensions being the smallest power of 2 possible just to avoid edge cases and off-by-one errors. The loop then pops the top of the queue and checks it; if that cube's size is 1 (i.e., it's a point), then it's the answer to the problem. If not, then we divide that into eight subcubes (dividing each dimension in half) and add those to the queue.

We can't just recurse into a subcube, because the greedy choice may be wrong; imagine if one subcube has five nanobots that don't intersect, while another subcube has two nanobots that do intersect.

UPDATE: As I said earlier, the original version of this code makes the first cube span the center of every nanobot, which does cut out points within the range of the bots on the edge. I assumed this wouldn't matter and the maximal intersection point would occur in the middle, which does in fact appear to be the case in my input, but this definitely isn't true in general. As a hilarious counterexample, take a single nanobot at (1, 0, 0) with radius 1. The minimal intersection point will be the origin (0, 0, 0), but the only point that would be checked is the bot's center. Oops!

Luckily, fixing this is as simple as adding & subtracting the radius to the initial cube's bounds as necessary. This actually didn't affect the runtime on my puzzle data at all because, due to the power of 2 thing, the initial cube was still the same size even with the larger required bounds.
