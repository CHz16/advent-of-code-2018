#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "23.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

struct Nanobot {
    let x: Int, y: Int, z: Int
    let radius: Int

    func covers(_ nanobot: Nanobot) -> Bool {
        var distance = abs(x - nanobot.x) + abs(y - nanobot.y)
        distance += abs(z - nanobot.z) // split up because the compiler would just hang forever lmao
        return (distance <= radius)
    }
}


var nanobots: [Nanobot] = []
var largestRadius = Int.min, largestRadiusIndex = -1
input.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: " ").compactMap { Int($0) }
    let nanobot = Nanobot(x: components[0], y: components[1], z: components[2], radius: components[3])
    nanobots.append(nanobot)

    if nanobot.radius > largestRadius {
        largestRadius = nanobot.radius
        largestRadiusIndex = nanobots.count - 1
    }
}
print(nanobots[largestRadiusIndex])

// A robot is considered in range of itself
var nanobotsInRange = 0
for nanobot in nanobots {
    if nanobots[largestRadiusIndex].covers(nanobot) {
        nanobotsInRange += 1
    }
}
print(nanobotsInRange)


// Part 2

print("--------------")

struct Cube {
    let x, y, z: Int
    let size: Int

    let maxX, maxY, maxZ: Int
    let distance: Int

    init(x: Int, y: Int, z: Int, size: Int) {
        self.x = x
        self.y = y
        self.z = z
        self.size = size

        // Done here instead of as computed properties because the next block uses these
        let maxX = x + size - 1, maxY = y + size - 1, maxZ = z + size - 1
        self.maxX = maxX
        self.maxY = maxY
        self.maxZ = maxZ

        // Manhattan distance from the cube to the origin
        // Find the closest point on the cube to the origin in each dimension in turn and then add those up, this might work
        let originDistanceX = (x < 0 && maxX > 0) ? 0 : min(abs(x), abs(maxX))
        let originDistanceY = (y < 0 && maxY > 0) ? 0 : min(abs(y), abs(maxY))
        let originDistanceZ = (z < 0 && maxZ > 0) ? 0 : min(abs(z), abs(maxZ))
        self.distance = originDistanceX + originDistanceY + originDistanceZ
    }

    func subcubes() -> [Cube] {
        let halfSize = size / 2
        return [
            Cube(x: x, y: y, z: z, size: halfSize),
            Cube(x: x + halfSize, y: y, z: z, size: halfSize),
            Cube(x: x, y: y + halfSize, z: z, size: halfSize),
            Cube(x: x, y: y, z: z + halfSize, size: halfSize),
            Cube(x: x + halfSize, y: y + halfSize, z: z, size: halfSize),
            Cube(x: x + halfSize, y: y, z: z + halfSize, size: halfSize),
            Cube(x: x, y: y + halfSize, z: z + halfSize, size: halfSize),
            Cube(x: x + halfSize, y: y + halfSize, z: z + halfSize, size: halfSize)
        ]
    }
}

extension Nanobot {
    func overlaps(_ cube: Cube) -> Bool {
        var distanceToCube = 0
        if x < cube.x {
            distanceToCube += cube.x - x
        } else if x > cube.maxX {
            distanceToCube += x - cube.maxX
        }
        if y < cube.y {
            distanceToCube += cube.y - y
        } else if y > cube.maxY {
            distanceToCube += y - cube.maxY
        }
        if z < cube.z {
            distanceToCube += cube.z - z
        } else if z > cube.maxZ {
            distanceToCube += z - cube.maxZ
        }
        return (distanceToCube <= radius)
    }
}



class Node {
    let cube: Cube
    let score: Int
    var next: Node?

    init(_ cube: Cube) {
        self.cube = cube

        var score = 0
        for nanobot in nanobots {
            if nanobot.overlaps(cube) {
                score += 1
            }
        }
        self.score = score
    }

    static func < (lhs: Node, rhs: Node) -> Bool {
        if lhs.score != rhs.score {
            return lhs.score < rhs.score // higher score is more desirable
        } else if lhs.cube.distance != rhs.cube.distance {
            return lhs.cube.distance > rhs.cube.distance // smaller distance is more desirable
        } else {
            return lhs.cube.size > rhs.cube.size // smaller size is more desirable
        }
    }
}

var xMin = Int.max, xMax = Int.min
var yMin = Int.max, yMax = Int.min
var zMin = Int.max, zMax = Int.min
for nanobot in nanobots {
    xMin = min(xMin, nanobot.x - nanobot.radius)
    xMax = max(xMax, nanobot.x - nanobot.radius)
    yMin = min(yMin, nanobot.y - nanobot.radius)
    yMax = max(yMax, nanobot.y + nanobot.radius)
    zMin = min(zMin, nanobot.z + nanobot.radius)
    zMax = max(zMax, nanobot.z + nanobot.radius)
}

// Just to avoid any possibility of edge cases and off-by-one errors, we'll make every cube a power of 2
var initialCubeSize = 1
while (xMin + initialCubeSize - 1) < xMax || (yMin + initialCubeSize - 1) < yMax || (zMin + initialCubeSize - 1) < zMax {
    initialCubeSize *= 2
}

var queueHead: Node? = Node(Cube(
    x: (xMin + xMax - initialCubeSize) / 2,
    y: (yMin + yMax - initialCubeSize) / 2,
    z: (zMin + zMax - initialCubeSize) / 2,
    size: initialCubeSize
))
while let node = queueHead {
    let cube = node.cube
    if cube.size == 1 {
        print(node.score, cube)
        print(cube.distance)
        break
    }
    queueHead = node.next

    for subcube in cube.subcubes() {
        let newNode = Node(subcube)
        if queueHead == nil {
            queueHead = newNode
            continue
        } else if queueHead! < newNode {
            newNode.next = queueHead
            queueHead = newNode
            continue
        }

        var inserted = false
        var prev = queueHead!
        while let next = prev.next {
            if next < newNode {
                prev.next = newNode
                newNode.next = next
                inserted = true
                break
            }
            prev = next
        }
        if !inserted {
            prev.next = newNode
        }
    }
}
