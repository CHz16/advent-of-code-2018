#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "15.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 2

struct Point: Comparable {
    let x, y: Int

    init(_ x: Int, _ y: Int) {
        self.x = x
        self.y = y
    }

    static func < (lhs: Point, rhs: Point) -> Bool {
        if lhs.y != rhs.y {
            return lhs.y < rhs.y
        } else {
            return lhs.x < rhs.x
        }
    }

    func adjacents() -> [Point] {
        return [Point(x - 1, y), Point(x + 1, y), Point(x, y - 1), Point(x, y + 1)]
    }
}

struct Unit: Comparable {
    let x, y: Int
    var hitPoints: Int
    let isElf: Bool

    init(x: Int, y: Int, isElf: Bool) {
        self.x = x
        self.y = y
        self.hitPoints = 200
        self.isElf = isElf
    }

    static func < (lhs: Unit, rhs: Unit) -> Bool {
        if lhs.y != rhs.y {
            return lhs.y < rhs.y
        } else {
            return lhs.x < rhs.x
        }
    }

    func adjacents() -> [Point] {
        return Point(x, y).adjacents()
    }

    func moved(_ newX: Int, _ newY: Int) -> Unit {
        var newUnit = Unit(x: newX, y: newY, isElf: isElf)
        newUnit.hitPoints = hitPoints
        return newUnit
    }

    func attacked(_ damage: Int) -> Unit? {
        if hitPoints <= damage {
            return nil
        } else {
            var newUnit = Unit(x: x, y: y, isElf: isElf)
            newUnit.hitPoints = hitPoints - damage
            return newUnit
        }
    }
}

enum State { case Wall, Unvisited, Visited, Target }

var walls: [[Bool]] = []
var basePathingGrid: [[State]] = []
var units: [Unit] = []
input.enumerateLines { (line, stop) in
    var wallsRow: [Bool] = Array(repeating: false, count: line.count)
    var pathingRow: [State] = Array(repeating: .Unvisited, count: line.count)
    for (x, char) in line.enumerated() {
        if char == "#" {
            wallsRow[x] = true
            pathingRow[x] = .Wall
        }
        if char == "G" {
            units.append(Unit(x: x, y: walls.count, isElf: false))
        } else if char == "E" {
            units.append(Unit(x: x, y: walls.count, isElf: true))
        }
    }
    walls.append(wallsRow)
    basePathingGrid.append(pathingRow)
}

var elfDamage = 2
var round = 0
var unitsCopy = units
outer: while true {
    elfDamage += 1
    print("elf damage:", elfDamage)
    units = unitsCopy

    round = 0
    round: while true {
        units = units.sorted()
        var i = 0
        while i < units.count {
            if (units.filter { $0.isElf }.count) == 0 || (units.filter { !$0.isElf }.count) == 0 {
                break round
            }

            var unit = units[i]

            // Check if the unit is already adjacent to an enemy
            var isAlreadyAdjacent = false
            for adjacent in unit.adjacents() {
                isAlreadyAdjacent = isAlreadyAdjacent || !(units.filter { $0.isElf != unit.isElf && $0.x == adjacent.x && $0.y == adjacent.y }.isEmpty)
            }

            // If not, then do a BFS and move
            if !isAlreadyAdjacent {
                var pathingGrid = basePathingGrid
                var frontier: [Point] = []
                for otherUnit in units {
                    pathingGrid[otherUnit.y][otherUnit.x] = .Wall
                    if otherUnit.isElf != unit.isElf {
                        frontier.append(contentsOf: otherUnit.adjacents())
                    }
                }
                for target in unit.adjacents() {
                    if pathingGrid[target.y][target.x] == .Unvisited {
                        pathingGrid[target.y][target.x] = .Target
                    }
                }

                var target: Point?
                while !frontier.isEmpty {
                    let targets = frontier.filter { pathingGrid[$0.y][$0.x] == .Target }
                    if !targets.isEmpty {
                        target = targets.sorted()[0]
                        break
                    }

                    var newFrontier: [Point] = []
                    for point in frontier {
                        if pathingGrid[point.y][point.x] != .Unvisited {
                            continue
                        }
                        pathingGrid[point.y][point.x] = .Visited
                        newFrontier.append(contentsOf: point.adjacents())
                    }
                    frontier = newFrontier
                }

                if let target = target {
                    units[i] = unit.moved(target.x, target.y)
                }
            }

            // Attack
            unit = units[i] // refresh if it moved
            let adjacents = unit.adjacents()
            var targets: [Int] = []
            j: for j in 0..<units.count {
                if unit.isElf == units[j].isElf {
                    continue
                }
                for adjacent in adjacents {
                    if units[j].x == adjacent.x && units[j].y == adjacent.y {
                        targets.append(j)
                        continue j
                    }
                }
            }
            if !targets.isEmpty {
                let targetIndex = targets.sorted {
                    if units[$0].hitPoints == units[$1].hitPoints {
                        return units[$0] < units[$1]
                    } else {
                        return units[$0].hitPoints < units[$1].hitPoints
                    }
                }[0]
                if let attackedTarget = units[targetIndex].attacked(unit.isElf ? elfDamage : 3) {
                    units[targetIndex] = attackedTarget
                } else {
                    if units[targetIndex].isElf {
                        print("elf died on round", round)
                        continue outer
                    }
                    units.remove(at: targetIndex)
                    if targetIndex < i {
                        i -= 1
                    }
                }
            }

            i += 1
        }
        round += 1
    }

    break
}

let remainingHitPoints = units.reduce(0) { $0 + $1.hitPoints }
print(units[0].isElf ? "Elves" : "Goblins", "win")
print(round, "*", remainingHitPoints, "=", round * remainingHitPoints)
