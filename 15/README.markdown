Day 15: Beverage Bandits
========================

https://adventofcode.com/2018/day/15

Time for more breadth-first search! Part 2 is just wrapping part 1 in an additional loop that increases the amount of damage elves do.

I started this one like two and a half hours late and still came in 462nd place on part 1 and 397th place on part 2, so I guess this one was pretty hard!

I guess I could talk a little about the BFS technique here, because the puzzle says the chosen target should be the closest square adjacent to an enemy, with the tiebreaking reading order, and then to move one square along a path to that target, where if multiple equal-length paths are possible then the tiebreaker is whichever possible first move is first in reading order. And my code does absolutely none of that: it doesn't find the closest enemy, doesn't calculate distances, and doesn't even find paths.

The trick is that it does the BFS in *reverse*: instead of searching from a unit to all the enemies, it searches from all the enemies to the squares adjacent to the unit. At the end of one iteration, we've found all the squares where the unit could be to reach an enemy in one move, and then so on with two and three and four. If at any point we start an iteration and a square that is adjacent to our unit is in the frontier, then the frontier will contain *all* points adjacent to the unit along a shortest path to an enemy, which is all that we care about. Then we can just figure out which one is first in reading order.

Update: just realized that this procedure can pick the wrong direction lmao, but I guess it didn't actually end up being a problem in any of the test inputs I used or my puzzle input. As an example, take this grid:

```
######
#.G..#
#.##.#
#.#..#
#..E##
######
```

If it's the elf's turn to move, my BFS here will start from the goblin and reach both squares adjacent to the elf simultaneously. Then, it'll choose between them in reading order and go up. However, according to the puzzle spec, the square to the left of the goblin should be the elf's target, in which case it should move left instead. Whoopsie!
