//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "4", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

struct Nap {
    let start, end: Int
    var duration: Int { return end - start }
}

var naps: [Int: [Nap]] = [:]
var guardID = 0, napStart = -1
input.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: " ")
    if components[3] == "Guard" {
        guardID = Int(components[4])!
        if naps[guardID] == nil {
            naps[guardID] = []
        }
    } else if napStart == -1 {
        napStart = Int(components[2])!
    } else {
        let nap = Nap(start: napStart, end: Int(components[2])!)
        naps[guardID]!.append(nap)
        napStart = -1
    }
}

let napCharts = naps.mapValues { (naps) -> [Int] in
    var chart = Array(repeating: 0, count: 60)
    for nap in naps {
        for m in nap.start..<nap.end {
            chart[m] += 1
        }
    }
    return chart
}

let minutesNapped = naps.map { ($0, $1.reduce(0) { $0 + $1.duration }) }
print(minutesNapped)
let nappingestGuardID = minutesNapped.max { $0.1 < $1.1 }!.0
print("guard", nappingestGuardID)

let chart = napCharts[nappingestGuardID]!
print(chart)
let mostMinutesSlept = chart.max()!, sleepingestMinute = chart.firstIndex(of: mostMinutesSlept)!
print(mostMinutesSlept, "@ minute", sleepingestMinute)
print(nappingestGuardID * sleepingestMinute)


// Part 2

print("--------------")

let mostSleptAtMinutes = napCharts.mapValues { $0.max()! }
print(mostSleptAtMinutes)
let mostConsistentlyNappingGuardID = mostSleptAtMinutes.max { $0.1 < $1.1 }!.0
print("guard", mostConsistentlyNappingGuardID)

let secondChart = napCharts[mostConsistentlyNappingGuardID]!
print(secondChart)
let sleepTimes = mostSleptAtMinutes[mostConsistentlyNappingGuardID]!
let consistentlySleepingestMinute = secondChart.firstIndex(of: sleepTimes)!
print(sleepTimes, "@ minute", consistentlySleepingestMinute)
print(mostConsistentlyNappingGuardID * consistentlySleepingestMinute)
