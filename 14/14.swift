#!/usr/bin/env xcrun swift

import Cocoa

let input = 635041


// Part 1

var recipes = [3, 7]
var elf1 = 0, elf2 = 1
while recipes.count < (input + 10) {
    let newRecipe = recipes[elf1] + recipes[elf2]
    if newRecipe >= 10 {
        recipes.append(1)
    }
    recipes.append(newRecipe % 10)
    elf1 = (elf1 + recipes[elf1] + 1) % recipes.count
    elf2 = (elf2 + recipes[elf2] + 1) % recipes.count
}
let answer = recipes[input..<recipes.count].map { String($0) }.reduce("", +)
print(answer.count > 10 ? answer.dropLast() : answer)


// Part 2

print("--------------")

let inputLength = Int(log10(Float(input))) + 1
var currentSlice = 0, inputMod = 10
for i in 0..<(inputLength-1) {
    currentSlice = currentSlice * 10 + recipes[i]
    inputMod *= 10
}
var lastIndexChecked = inputLength - 1

outer: while true {
    while lastIndexChecked < recipes.count {
        currentSlice = (currentSlice * 10) % inputMod + recipes[lastIndexChecked]
        lastIndexChecked += 1
        if currentSlice == input {
            print(lastIndexChecked - inputLength)
            break outer
        }
    }

    let newRecipe = recipes[elf1] + recipes[elf2]
    if newRecipe >= 10 {
        recipes.append(1)
    }
    recipes.append(newRecipe % 10)
    elf1 = (elf1 + recipes[elf1] + 1) % recipes.count
    elf2 = (elf2 + recipes[elf2] + 1) % recipes.count
}
