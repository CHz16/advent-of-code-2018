Day 14: Chocolate Charts
========================

https://adventofcode.com/2018/day/14

Very simple one here with an easy process for adding numbers to a list in part one, and then checking for a subsequence in that list in part two. This puzzle involves accessing many items by index and adding one or two items to the end always, so an array is the correct data structure this time (see day 9).

I got caught up on part 2 because (1) it took me a while to figure out what it was actually asking for, and (2) my code doesn't properly handle inputs starting with the number 0, and one of the example inputs does, so it took me a while to figure out what the problem was there. My puzzle input doesn't, so it's fine.

* Part 1: 134th place (10:00)
* Part 2: 467th place (38:46)
