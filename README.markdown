Advent of Code 2018
===================

These are my solutions in Swift to [Advent of Code 2018](https://adventofcode.com/2018), a kind of fun series of 25 programming puzzles that ran in December 2018. I figured I'd throw these up on the web somewhere, because why not. If I was competing for a leaderboard spot that day (that is, I started coding at 9 PM Pacific), then the readme file for that day will give my time and place.

While the challenges had no time limit, there was a leaderboard that showed (essentially) the fastest 100 solvers of each day's problem, so naturally I tried to solve them as quickly as possible. So, most of the solutions here are simply what left my fingers the fastest. That's speedcoding for you. I hadn't really written much Swift or done much puzzle programming this year, so I was pretty rusty going into this one.

I started writing every single challenge in a playground, because they're fantastic tools, but eventually every puzzle became something that would loop thousands of times, and timelines collecting all those intermediate values massively slow things down, so halfway through I just started using command-line scripts for everything.

Every user had their puzzle inputs randomly selected from a pool of possibilities, so the inputs you see in these files may not be the same ones you'd get were you to sign up. I also tend to preprocess them in a text editor to make them way easier to parse.

I've included [Marcin Krzyżanowski's CryptoSwift framework](https://github.com/krzyzanowskim/CryptoSwift) because 2015 and 2016 required MD5 hashing, but 2017 didn't so we'll see if 2018 doesn't either. (e: yep didn't use it again) Every other bit of code in this repo is something I cranked out during this contest, though sometimes based heavily on something I've written before.

* Best placement for any part: day 19, part 2 (20th place)
* Days with later optimizations or bugfixes: 11, 17
* Days that are actually incorrect: 15, 16, 24
* Favorite solution: day 20
* I cheated on: day 23

*See also: [2015](https://bitbucket.org/CHz16/advent-of-code-2015), [2016](https://bitbucket.org/CHz16/advent-of-code-2016), [2017](https://bitbucket.org/CHz16/advent-of-code-2017), [2019](https://bitbucket.org/CHz16/advent-of-code-2019), [2020](https://bitbucket.org/CHz16/advent-of-code-2020), [2021](https://bitbucket.org/CHz16/advent-of-code-2021), [2022](https://bitbucket.org/CHz16/advent-of-code-2022)*
