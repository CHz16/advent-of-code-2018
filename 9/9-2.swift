#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "9.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 2

class Marble {
    let score: Int
    var next: Marble?
    weak var prev: Marble?
    init(_ score: Int) {
        self.score = score
        self.next = self
        self.prev = self
    }
}

input.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: " ").compactMap { Int($0) }
    let players = components[0], maxMarble = components[1] * 100
    print(components)

    var scores = Array(repeating: 0, count: players)
    var currentMarble = Marble(0), currentPlayer = 0
    for i in 1...maxMarble {
        if i % 23 == 0 {
            for _ in 0..<7 {
                currentMarble = currentMarble.prev!
            }
            scores[currentPlayer] += i + currentMarble.score
            currentMarble.prev!.next = currentMarble.next!
            currentMarble.next!.prev = currentMarble.prev
            currentMarble = currentMarble.next!
        } else {
            currentMarble = currentMarble.next!

            let newMarble = Marble(i)
            newMarble.prev = currentMarble
            newMarble.next = currentMarble.next!
            currentMarble.next!.prev = newMarble
            currentMarble.next = newMarble

            currentMarble = newMarble
        }
        currentPlayer = (currentPlayer + 1) % players
    }
    print(scores.max()!)
}
