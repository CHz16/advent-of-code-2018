Day 9: Marble Mania
===================

https://adventofcode.com/2018/day/9

I am just consistently picking the wrong approach for part 2s this year and losing a bunch of time lmao

For part 1 I just implemented this straightforwardly with a Swift array, but then part 2 ups the count by a lot and so array insertion/deletion performance gets real bad. My first approach was to waste a whole bunch of time trying to find a mathematically nice way to figure out which items we'd be using in each step so I could just calculate scores directly without needing to maintain the array. In the background, I kept the program running just in case it finished before I did.

45 minutes later, I had decided to just throw a linked list at the problem instead, and coded that up, and it finished in 14 seconds. The original part 1 program had still not terminated by this point. Sigh.

For the record, I'm aware that the linked list implementation here is not good w.r.t. leaking nodes and edge cases and you shouldn't use it for anything. If I use it again in a future problem, I might clean it up with proper & correct `.insert` and `.delete` methods on the class. (e: I didn't)

* Part 1: 350th place (26:47)
* Part 2: 574th place (1:10:50)
