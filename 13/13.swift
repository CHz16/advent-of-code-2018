#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "13.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Parts 1 & 2

enum Track {
    case None, Horizontal, Vertical, Intersection, ClockwiseTurn, CounterclockwiseTurn
}

enum Direction {
    case Up, Down, Left, Right

    func turned(_ turnState: TurnState) -> Direction {
        switch turnState {
        case .Left:
            if self == .Up {
                return .Left
            } else if self == .Left {
                return .Down
            } else if self == .Down {
                return .Right
            } else {
                return .Up
            }
        case .Straight:
            return self
        case .Right:
            if self == .Up {
                return .Right
            } else if self == .Right {
                return .Down
            } else if self == .Down {
                return .Left
            } else {
                return .Up
            }
        }
    }
}

enum TurnState {
    case Left, Straight, Right
}

struct Cart {
    let x, y: Int
    let direction: Direction
    let nextTurnState: TurnState

    func moved(along track: Track) -> Cart {
        let newDirection: Direction
        let newNextTurnState: TurnState

        switch track {
        case .Intersection:
            newDirection = direction.turned(nextTurnState)
            if nextTurnState == .Left {
                newNextTurnState = .Straight
            } else if nextTurnState == .Straight {
                newNextTurnState = .Right
            } else {
                newNextTurnState = .Left
            }
        case .ClockwiseTurn:
            switch direction {
            case .Up:
                newDirection = .Right
            case .Right:
                newDirection = .Up
            case .Down:
                newDirection = .Left
            case .Left:
                newDirection = .Down
            }
            newNextTurnState = nextTurnState
        case .CounterclockwiseTurn:
            switch direction {
            case .Up:
                newDirection = .Left
            case .Right:
                newDirection = .Down
            case .Down:
                newDirection = .Right
            case .Left:
                newDirection = .Up
            }
            newNextTurnState = nextTurnState
        case .Horizontal, .Vertical, .None:
            newDirection = direction
            newNextTurnState = nextTurnState
        }

        let horizontalDelta: Int
        switch newDirection {
        case .Up, .Down:
            horizontalDelta = 0
        case .Right:
            horizontalDelta = 1
        case .Left:
            horizontalDelta = -1
        }

        let verticalDelta: Int
        switch newDirection {
        case .Left, .Right:
            verticalDelta = 0
        case .Up:
            verticalDelta = -1
        case .Down:
            verticalDelta = 1
        }

        return Cart(x: x + horizontalDelta, y: y + verticalDelta, direction: newDirection, nextTurnState: newNextTurnState)
    }
}


let inputLines = input.components(separatedBy: "\n")
let tracksWidth = inputLines.map { $0.count }.max()!

var tracks = Array(repeating: Array(repeating: Track.None, count: tracksWidth), count: inputLines.count - 1)
var carts: [Cart] = []
for (y, line) in inputLines.enumerated() {
    for (x, char) in line.enumerated() {
        switch char {
        case "-":
            tracks[y][x] = .Horizontal
        case "|":
            tracks[y][x] = .Vertical
        case "/":
            tracks[y][x] = .ClockwiseTurn
        case "\\":
            tracks[y][x] = .CounterclockwiseTurn
        case "+":
            tracks[y][x] = .Intersection
        case "v":
            tracks[y][x] = .Vertical
            carts.append(Cart(x: x, y: y, direction: .Down, nextTurnState: .Left))
        case "^":
            tracks[y][x] = .Vertical
            carts.append(Cart(x: x, y: y, direction: .Up, nextTurnState: .Left))
        case "<":
            tracks[y][x] = .Horizontal
            carts.append(Cart(x: x, y: y, direction: .Left, nextTurnState: .Left))
        case ">":
            tracks[y][x] = .Horizontal
            carts.append(Cart(x: x, y: y, direction: .Right, nextTurnState: .Left))
        default:
            break
        }
    }
}

while carts.count > 1 {
    carts = carts.sorted {
        if $0.y == $1.y {
            return $0.x < $1.x
        } else {
            return $0.y < $1.y
        }
    }

    var i = 0
    while i < carts.count {
        carts[i] = carts[i].moved(along: tracks[carts[i].y][carts[i].x])
        for j in 0..<carts.count {
            if i == j {
                continue
            }
            if carts[i].x == carts[j].x && carts[i].y == carts[j].y {
                print("crash at \(carts[i].x),\(carts[i].y)")
                carts.remove(at: max(i, j))
                carts.remove(at: min(i, j))
                if j > i {
                    i -= 1
                } else {
                    i -= 2
                }
                break
            }
        }
        i += 1
    }
}
print("last car at \(carts[0].x),\(carts[0].y)")
