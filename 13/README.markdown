Day 13: Mine Cart Madness
=========================

https://adventofcode.com/2018/day/13

Did pretty decently on part 1, and then flopped part 2 because of a straight up bug in my code that took a while to find lmao

The problem is that I was trying to be clever in my collision loop and only checked if a moved cart had collided with carts that had moved already and not every cart in the list. This turns out to miss collisions if a cart moves into the position occupied by a cart that hasn't moved yet, as it won't check collision with that second cart then, and then after the second one moves it won't have collided with the first one any more. Oops!

* Part 1: 174th place (38:36)
* Part 2: 598th place (1:36:34)
