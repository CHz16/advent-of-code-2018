#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "5.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

func reducePolymer(_ polymer: String) -> String {
    var polymer = polymer
    var index = polymer.startIndex
    while index != polymer.index(before: polymer.endIndex) {
        let nextIndex = polymer.index(after: index)
        let left = String(polymer[index]), right = String(polymer[nextIndex])

        if (left.lowercased() == right && left == right.uppercased()) || (left.uppercased() == right && left == right.lowercased()) {
            polymer.remove(at: nextIndex)
            polymer.remove(at: index)
            if index != polymer.startIndex {
                index = polymer.index(before: index)
            }
        } else {
            index = nextIndex
        }
    }
    return polymer
}

let polymer = String(input.dropLast()), reducedPolymer = reducePolymer(polymer)
print(reducedPolymer)
print(reducedPolymer.count)


// Part 2

print("--------------")

let units = Set(polymer.lowercased())
var shortestPolymerLength = Int.max
for unit in units {
    let uppercaseUnit = Character(String(unit).uppercased())
    let reducedFilteredPolymer = reducePolymer(polymer.filter { $0 != unit && $0 != uppercaseUnit })
    print(unit, reducedFilteredPolymer.count)

    if reducedFilteredPolymer.count < shortestPolymerLength {
        shortestPolymerLength = reducedFilteredPolymer.count
    }
}
print(shortestPolymerLength)
