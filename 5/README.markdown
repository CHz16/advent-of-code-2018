Day 5: Alchemical Reduction
===========================

https://adventofcode.com/2018/day/5

Just simple string manipulation again, remove pairs of characters from the string if they match a certain criterion, and then do that transformation over the string after removing all of one type of character from that string.

* Part 1: 439th place (10:36)
* Part 2: 477th place (20:44)
