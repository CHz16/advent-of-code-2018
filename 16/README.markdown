Day 16: Chronal Classification
==============================

https://adventofcode.com/2018/day/16

Well here's the VM puzzle this year. Part 1 is just run every single operation on every single sample and count up which ones produce the expected output, not bad. During this loop, every time a sample doesn't work for a given opcode/operation pair, I save that data for later.

In part 2, I use that data to programmatically figure out which opcode corresponds to which operation. The loop just scans through the data from part 1 and finds an operation that can only correspond to a single opcode, saves that for later, and then marks that opcode as invalid for every other operation. Then it repeats that loop with the new information until everything is assigned.

I thought I might have to code another logic step too: scanning the data for opcodes that can only correspond to a single operation. However, on my puzzle input at least, this wasn't necessary!

Just to describe the pruning steps a bit more fully, imagine this simplified set of operations and opcodes:

```
  0 1 2 3
+ F F T F
* T T T T
> T F T T
= F F T T
```

In this sample, `+` only has the opcode 2 available to it, so obviously we can assign that; this is the logic step that I coded. But also notice that the only instruction that can possibly have the opcode 1 is `*`, so we can assign that one too; this is the one that I didn't need to add.
