#!/usr/bin/env xcrun swift

import Cocoa

let url1 = URL(fileURLWithPath: "16-1.txt")
let input1 = try String(contentsOf: url1, encoding: String.Encoding.utf8)
let url2 = URL(fileURLWithPath: "16-2.txt")
let input2 = try String(contentsOf: url2, encoding: String.Encoding.utf8)


// Part 1

enum Operation: Int, CaseIterable { case addRegister, addImmediate, multiplyRegister, multiplyImmediate, bitwiseAndRegister, bitwiseAndImmediate, bitwiseOrRegister, bitwiseOrImmediate, setRegister, setImmediate, greaterThanImmediateRegister, greaterThanRegisterImmediate, greaterThanRegisterRegister, equalImmediateRegister, equalRegisterImmediate, equalRegisterRegister }

func execute(operation: Operation, withArguments arguments: [Int], on registers: [Int]) -> [Int] {
    var registers = registers
    switch operation {
    case .addRegister:
        registers[arguments[2]] = registers[arguments[0]] + registers[arguments[1]]
    case .addImmediate:
        registers[arguments[2]] = registers[arguments[0]] + arguments[1]
    case .multiplyRegister:
        registers[arguments[2]] = registers[arguments[0]] * registers[arguments[1]]
    case .multiplyImmediate:
        registers[arguments[2]] = registers[arguments[0]] * arguments[1]
    case .bitwiseAndRegister:
        registers[arguments[2]] = registers[arguments[0]] & registers[arguments[1]]
    case .bitwiseAndImmediate:
        registers[arguments[2]] = registers[arguments[0]] & arguments[1]
    case .bitwiseOrRegister:
        registers[arguments[2]] = registers[arguments[0]] | registers[arguments[1]]
    case .bitwiseOrImmediate:
        registers[arguments[2]] = registers[arguments[0]] | arguments[1]
    case .setRegister:
        registers[arguments[2]] = registers[arguments[0]]
    case .setImmediate:
        registers[arguments[2]] = arguments[0]
    case .greaterThanImmediateRegister:
        registers[arguments[2]] = (arguments[0] > registers[arguments[1]]) ? 1 : 0
    case .greaterThanRegisterImmediate:
        registers[arguments[2]] = (registers[arguments[0]] > arguments[1]) ? 1 : 0
    case .greaterThanRegisterRegister:
        registers[arguments[2]] = (registers[arguments[0]] > registers[arguments[1]]) ? 1 : 0
    case .equalImmediateRegister:
        registers[arguments[2]] = (arguments[0] == registers[arguments[1]]) ? 1 : 0
    case .equalRegisterImmediate:
        registers[arguments[2]] = (registers[arguments[0]] == arguments[1]) ? 1 : 0
    case .equalRegisterRegister:
        registers[arguments[2]] = (registers[arguments[0]] == registers[arguments[1]]) ? 1 : 0
    }
    return registers
}


var threePlusSamples = 0
var availableOpcodes = Array(repeating: Array(repeating: true, count: Operation.allCases.count), count: Operation.allCases.count)
input1.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: " ").compactMap { Int($0) }
    let registersBefore = Array(components[0..<4]), registersAfter = Array(components[8..<12])
    let opcode = components[4], arguments = Array(components[5..<8])

    var validOpcodes = 0
    for operation in Operation.allCases {
        let result = execute(operation: operation, withArguments: arguments, on: registersBefore)
        if result == registersAfter {
            validOpcodes += 1
        } else {
            availableOpcodes[operation.rawValue][opcode] = false
        }
    }
    if validOpcodes >= 3 {
        threePlusSamples += 1
    }
}
print(threePlusSamples)


// Part 2

print("--------------")

var operations: [Int: Operation] = [:]
while operations.count < Operation.allCases.count {
    var opcodesToPrune: [Int] = []

    // Assign any operation with only one possible opcode
    for operation in Operation.allCases {
        if (availableOpcodes[operation.rawValue].filter { $0 }).count == 1 {
            let opcode = availableOpcodes[operation.rawValue].firstIndex(of: true)!
            operations[opcode] = operation
            opcodesToPrune.append(opcode)
            availableOpcodes[operation.rawValue] = []
            print(opcode, "->", operation)
        }
    }

    // Prune the opcodes we assigned
    for opcode in opcodesToPrune {
        for operation in Operation.allCases {
            if !availableOpcodes[operation.rawValue].isEmpty {
                availableOpcodes[operation.rawValue][opcode] = false
            }
        }
    }
}

var registers = [0, 0, 0, 0]
input2.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: " ").compactMap { Int($0) }
    registers = execute(operation: operations[components[0]]!, withArguments: Array(components[1..<4]), on: registers)
}
print(registers)
