Day 20: A Regular Map
=====================

https://adventofcode.com/2018/day/20

I decided to triple super omega overengineer this one today for no particularly compelling reason!

So first we [build a nondeterministic finite automaton from the regex](https://en.wikipedia.org/wiki/Thompson%27s_construction). From there, we can build the topology of the facility by doing a depth-first search over the NFA to generate every possible "distinct" path (coalescing paths that end up in the same room with the same NFA state). From there, it's just a simple breadth-first search to answer the two parts of the question.

* Part 1: 538th place (2:48:44)
* Part 2: 495th place (2:54:44)
