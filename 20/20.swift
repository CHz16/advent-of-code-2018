#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "20.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Parts 1 & 2

typealias State = [String: Int]

var nfa: [State] = []
func parse() {
    func parse(startIndex: String.Index, endIndex: String.Index, currentState: Int, destination: Int) {
        // Start and end
        if input[startIndex] == "^" {
            parse(startIndex: input.index(after: startIndex), endIndex: endIndex, currentState: currentState, destination: destination)
            return
        } else if input[startIndex] == "$" {
            nfa[currentState][""] = destination
            return
        } else if startIndex == endIndex {
            nfa[currentState][""] = destination
            return
        }

        // Unions
        if input[startIndex] == "(" {
            nfa.append(State())
            let unionDestination = nfa.count - 1

            var depth = 1
            var alternateStart = input.index(after: startIndex)
            var scanIndex = alternateStart
            while depth > 0 {
                if input[scanIndex] == "(" {
                    depth += 1
                } else if input[scanIndex] == ")" {
                    depth -= 1
                }

                if (depth == 1 && input[scanIndex] == "|") || (depth == 0 && input[scanIndex] == ")") {
                    parse(startIndex: alternateStart, endIndex: scanIndex, currentState: currentState, destination: unionDestination)
                    alternateStart = input.index(after: scanIndex)
                }
                scanIndex = input.index(after: scanIndex)
            }

            parse(startIndex: scanIndex, endIndex: endIndex, currentState: unionDestination, destination: destination)
            return
        }

        // Directions
        var scanIndex = startIndex
        while input[scanIndex] == "N" || input[scanIndex] == "E" || input[scanIndex] == "S" || input[scanIndex] == "W" {
            scanIndex = input.index(after: scanIndex)
        }
        if scanIndex == endIndex {
            nfa[currentState][String(input[startIndex..<scanIndex])] = destination
        } else {
            nfa.append(State())
            let newCurrentState = nfa.count - 1
            nfa[currentState][String(input[startIndex..<scanIndex])] = newCurrentState
            parse(startIndex: scanIndex, endIndex: endIndex, currentState: newCurrentState, destination: destination)
        }
    }

    nfa.append(State())
    nfa.append(State())
    parse(startIndex: input.startIndex, endIndex: input.endIndex, currentState: 0, destination: 1)
}
parse()


struct Doors { var north = false, east = false, south = false, west = false }

var grid: [Int: [Int: Doors]] = [0: [0: Doors()]]
var visited = Set<String>()
var stack = [(x: 0, y: 0, state: 0)]
while let (x, y, state) = stack.popLast() {
    let hash = String(x) + "," + String(y) + "," + String(state)
    if visited.contains(hash) {
        continue
    }
    visited.insert(hash)

    for (directions, nextState) in nfa[state] {
        var currentX = x, currentY = y
        for direction in directions {
            var destinationX = currentX, destinationY = currentY
            if direction == "N" {
                destinationY -= 1
            } else if direction == "E" {
                destinationX += 1
            } else if direction == "S" {
                destinationY += 1
            } else {
                destinationX -= 1
            }

            if grid[destinationY] == nil {
                grid[destinationY] = [:]
                grid[destinationY]![destinationX] = Doors()
            } else if grid[destinationY]?[destinationX] == nil {
                grid[destinationY]![destinationX] = Doors()
            }

            if direction == "N" {
                grid[currentY]![currentX]!.north = true
                grid[destinationY]![destinationX]!.south = true
            } else if direction == "E" {
                grid[currentY]![currentX]!.east = true
                grid[destinationY]![destinationX]!.west = true
            } else if direction == "S" {
                grid[currentY]![currentX]!.south = true
                grid[destinationY]![destinationX]!.north = true
            } else {
                grid[currentY]![currentX]!.west = true
                grid[destinationY]![destinationX]!.east = true
            }

            currentX = destinationX
            currentY = destinationY
        }
        stack.append((x: currentX, y: currentY, state: nextState))
    }
}


var distance = 0
var part2Rooms = 0
var frontier = [(x: 0, y: 0)]
visited = Set<String>()
while !frontier.isEmpty {
    var newFrontier: [(x: Int, y: Int)] = []
    var visitedARoom = false
    for (x, y) in frontier {
        let hash = String(x) + "," + String(y)
        if visited.contains(hash) {
            continue
        }

        visited.insert(hash)
        visitedARoom = true
        if distance >= 1000 {
            part2Rooms += 1
        }

        let doors = grid[y]![x]!
        if doors.north {
            newFrontier.append((x: x, y: y - 1))
        }
        if doors.east {
            newFrontier.append((x: x + 1, y: y))
        }
        if doors.south {
            newFrontier.append((x: x, y: y + 1))
        }
        if doors.west {
            newFrontier.append((x: x - 1, y: y))
        }
    }
    frontier = newFrontier
    if visitedARoom {
        distance += 1
    }
}
print(distance - 1)
print("--------------")
print(part2Rooms)
