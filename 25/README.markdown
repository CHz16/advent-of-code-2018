Day 25: Four-Dimensional Adventure
==================================

https://adventofcode.com/2018/day/25

The traditional breather puzzle for the final day. I did this by just maintaining a list of constellations and going through each point in turn, adding it to the first constellation it's adjacent to and, if none, making a new constellation with that point in it. That was incorrect, because it's possible a point will be adjacent to more than one constellation, in which case you need to merge them into one. I figured that one out and fixed it pretty quickly, although not quickly enough to sneak onto the leaderboard.

* Part 1: 128th place (13:27)
* Part 2: 102nd place (13:32)
