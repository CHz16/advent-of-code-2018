#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "25.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

struct Point {
    let x, y, z, w: Int

    func neighbors(_ point: Point) -> Bool {
        var distance = abs(x - point.x) + abs(y - point.y)
        distance += abs(z - point.z) + abs(w - point.w)
        return (distance <= 3)
    }
}

var constellations: [[Point]] = []
input.enumerateLines { (line, stop) in
    let coordinates = line.components(separatedBy: " ").compactMap { Int($0) }
    let point = Point(x: coordinates[0], y: coordinates[1], z: coordinates[2], w: coordinates[3])

    var candidateConstellations: [Int] = []
    for i in 0..<constellations.count {
        if (!constellations[i].allSatisfy { !$0.neighbors(point) }) {
            candidateConstellations.append(i)
        }
    }

    if candidateConstellations.isEmpty {
        constellations.append([point])
    } else {
        constellations[candidateConstellations[0]].append(point)
        for mergeIndex in candidateConstellations.dropFirst().reversed() {
            constellations[candidateConstellations[0]].append(contentsOf: constellations[mergeIndex])
            constellations.remove(at: mergeIndex)
        }
    }
}
print(constellations.count)
