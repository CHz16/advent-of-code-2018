Day 24: Immune System Simulator 20XX
====================================

https://adventofcode.com/2018/day/24

Wow this one is way easier than yesterday's lmao. It's like day 15, where you have to run a complex simulation of a video game, except this one is simpler?? Literally no cleverness in this solution, just did it.

Part 2 is also basically the same as day 15 part 1: find the smallest value by which to increase the good guys' attack power so they win. This time I did it with an open-ended binary search; I made the totally unfounded assumption that once a boost results in a success, every boost after that will work too. I feel like this probably doesn't work in the general case, but I wanted to avoid brute forcing, because the given example's lowest boost is 1570.

... and it turned out that my lowest boost was 82, so I could've brute forced it. PLEASE

Anyway, the one tricky bit is that, with certain boosts, it's possible to get into a situation where no groups kill each other on a turn so the battle loops forever; this happened with me at a boost of 32. I didn't check the trace, but the possibilities seem like (1) no groups actually have enough attack power to kill a unit, or (2) all units are immune to the enemy units, so no one attacks. So there's a small check in there that's not in part 1 to see if any units were killed on the turn, and if not, combat immediately ends.

UPDATE: "I feel like this probably doesn't work in the general case" yeah I just realized five minutes after committing that that it doesn't, because of stalemates like I just described in the last paragraph lmao. It's probably possible to retool the binary search by ignoring stalemate boosts (first idea: range from the latest confirmed failure to earliest confirmed success, create a array with every boost value in the range, remove stalemate values from the array as they're encountered, adjust indices for the removal), but that seems very easy to mess up and again, brute force would've nailed this lmao
