#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "24.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 2

var boost = 0

struct Group {
    let isImmuneSystem: Bool
    var units: Int
    let hitPoints: Int
    let damage: Int, damageType: String
    let initiative: Int
    let weaknesses: [String], immunities: [String]

    var effectivePower: Int { return units * (damage + (isImmuneSystem ? boost : 0)) }
    var isDead: Bool { return units <= 0 }


    func damage(from attacker: Group) -> Int {
        if immunities.contains(attacker.damageType) {
            return 0
        } else if weaknesses.contains(attacker.damageType) {
            return 2 * attacker.effectivePower
        } else {
            return attacker.effectivePower
        }
    }

    mutating func attack(by attacker: Group) -> Int {
        let unitsKilled = self.damage(from: attacker) / hitPoints
        units -= unitsKilled
        return unitsKilled
    }
}


var groups: [Group] = []
input.enumerateLines { (line, stop) in
    var line = line
    var weaknesses: [String] = [], immunities: [String] = []
    if let resistsOpening = line.firstIndex(of: "(") {
        let resistsClosing = line.firstIndex(of: ")")!
        let resists = line[line.index(after: resistsOpening)..<resistsClosing].components(separatedBy: "; ")
        for resist in resists {
            let resistComponents = resist.components(separatedBy: " ")
            if resistComponents[0] == "weak" {
                weaknesses.append(resistComponents[2])
            } else if resistComponents[0] == "immune" {
                immunities.append(resistComponents[2])
            }
        }
        line.removeSubrange(resistsOpening...line.index(after: resistsClosing))
    }

    let components = line.components(separatedBy: " ")
    groups.append(Group(
        isImmuneSystem: components[0] == "Immune",
        units: Int(components[1])!,
        hitPoints: Int(components[5])!,
        damage: Int(components[13])!,
        damageType: components[14],
        initiative: Int(components[18])!,
        weaknesses: weaknesses,
        immunities: immunities
    ))
}
groups = groups.sorted { $0.initiative > $1.initiative }
let groupsCopy = groups

var results: [Int: Int] = [:]
var rangeMin = 1, rangeMax = Int.max
while true {
    groups = groupsCopy
    if rangeMax == Int.max {
        // Still probing ahead to find the first hit
        boost = rangeMin
    } else {
        boost = (rangeMin + rangeMax) / 2
    }

    var bailedOut = false
    while true {
        if groups.filter({ $0.isImmuneSystem }).isEmpty || groups.filter({ !$0.isImmuneSystem }).isEmpty {
            break
        }

        let targetingOrder = (0..<groups.count).sorted {
            if groups[$0].effectivePower != groups[$1].effectivePower {
                return groups[$0].effectivePower > groups[$1].effectivePower
            } else {
                return groups[$0].initiative != groups[$1].initiative
            }
        }

        var targets = Array(repeating: -1, count: groups.count)
        for i in targetingOrder {
            let attacker = groups[i]
            var maxDamage = Int.min, maxEffectivePower = Int.min
            for (j, targetGroup) in groups.enumerated() {
                if i == j || attacker.isImmuneSystem == targetGroup.isImmuneSystem || targets.contains(j) {
                    continue
                }
                let damage = targetGroup.damage(from: attacker)
                if damage > 0 && damage > maxDamage || (damage == maxDamage && targetGroup.effectivePower > maxEffectivePower) {
                    targets[i] = j
                    maxDamage = damage
                    maxEffectivePower = targetGroup.effectivePower
                }
            }
        }

        var unitsKilled = 0
        for (attackerIndex, targetIndex) in targets.enumerated() {
            if targetIndex != -1 && !groups[attackerIndex].isDead && !groups[targetIndex].isDead {
                unitsKilled += groups[targetIndex].attack(by: groups[attackerIndex])
            }
        }
        // Infinite loop check, in case we end up in a situation where two units can't kill each other because of immunities or something
        if unitsKilled == 0 {
            bailedOut = true
            break
        }

        groups = groups.filter { !$0.isDead }
    }

    let succeeded = !bailedOut && groups[0].isImmuneSystem
    if succeeded {
        print("succeeded at \(boost)")
        results[boost] = groups.map { $0.units }.reduce(0, +)
    } else {
        print("failed at \(boost)")
    }

    if rangeMax == Int.max {
        // Still probing for the first hit
        if !succeeded {
            // Probe forward by x2
            rangeMin *= 2
        } else {
            // Probing complete, set up the range for binary search
            rangeMax = rangeMin
            rangeMin /= 2
        }
    } else {
        // Adjust the bounds
        if !succeeded {
            rangeMin = boost
        } else {
            rangeMax = boost
        }
    }

    // Check for success
    if rangeMin == rangeMax - 1 {
        print("---")
        print("first success \(rangeMax)")
        print(results[rangeMax]!)
        break
    }
}

