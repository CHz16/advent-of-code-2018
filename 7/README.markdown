Day 7: The Sum of Its Parts
===========================

https://adventofcode.com/2018/day/7

Part 1 I did pretty well on. We first go through the data and record the number of prerequisites each step has and which steps that a given step opens up. Then, the algorithm goes through the list of steps in alphabetical order and finds the first step we haven't done with no prerequisites remaining, adding that step to the answer and subtracting 1 from each of its dependent steps.

Part 2 I messed up on because I disregarded the part of the problem where it says to use five workers; I assumed it would be two like the example and hardcoded that in. So, I had to tear that all apart and write a more general version, oops! The code is honestly neater this way anyway lmao

There's ambiguity in part 2 involving what to do with the answer if two workers finish their step at the same time; in my code I decided to print the simultaneously finished steps in alphabetical order. My assumption is that the input is tailored so this will never happen; mine wasn't at least.

* Part 1: 252nd place (14:30)
* Part 2: 640th place (1:05:21)
