//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "7", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

var prereqsLeft: [String: Int] = [:]
var dependents: [String: [String]] = [:]
input.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: " ")
    let step = components[7], prereq = components[1]

    prereqsLeft[step] = (prereqsLeft[step] ?? 0) + 1
    if prereqsLeft[prereq] == nil {
        prereqsLeft[prereq] = 0
    }

    if dependents[prereq] == nil {
        dependents[prereq] = []
    }
    dependents[prereq]!.append(step)
}
let prereqsLeftCopy = prereqsLeft
print(prereqsLeft)
print(dependents)

var answer = ""
outer: while true {
    for key in prereqsLeft.keys.sorted() {
        let value = prereqsLeft[key]!
        if value != 0 {
            continue
        }

        answer += key
        prereqsLeft[key] = -1
        if dependents[key] != nil {
            for dependent in dependents[key]! {
                prereqsLeft[dependent]! -= 1
            }
        }

        continue outer
    }

    break
}
print(answer)


// Part 2

print("--------------")

prereqsLeft = prereqsLeftCopy


let DURATIONS = ["A": 1, "B": 2, "C": 3, "D": 4, "E": 5, "F": 6, "G": 7, "H": 8, "I": 9, "J": 10, "K": 11, "L": 12, "M": 13, "N": 14, "O": 15, "P": 16, "Q": 17, "R": 18, "S": 19, "T": 20, "U": 21, "V": 22, "W": 23, "X": 24, "Y": 25, "Z": 26]
let EXTRA_DURATION = 60
let WORKERS = 5

var time = 0
var workerTasks: [String?] = Array(repeating: nil, count: WORKERS)
var workerTimesLeft: [Int?] = Array(repeating: nil, count: WORKERS)
answer = ""
while true {
    for i in 0..<WORKERS {
        if workerTasks[i] == nil {
            for key in prereqsLeft.keys.sorted() {
                let value = prereqsLeft[key]!
                if value == 0 {
                    print(time, "worker", i, "choosing", key)
                    workerTasks[i] = key
                    workerTimesLeft[i] = DURATIONS[key]! + EXTRA_DURATION
                    prereqsLeft[key] = -1
                    break
                }
            }
        }
    }

    if (workerTasks.filter { $0 != nil }.count) == 0 {
        break
    }

    let timeStep = workerTimesLeft.compactMap { $0 }.min()!
    time += timeStep
    for i in 0..<WORKERS {
        if workerTimesLeft[i] != nil {
            workerTimesLeft[i]! -= timeStep
        }
    }

    var completed = ""
    for i in 0..<WORKERS {
        if workerTimesLeft[i] != nil && workerTimesLeft[i]! == 0 {
            let key = workerTasks[i]!
            print(time, "worker", i, "finishing", key)
            completed += key
            if dependents[key] != nil {
                for dependent in dependents[key]! {
                    prereqsLeft[dependent]! -= 1
                }
            }
            workerTasks[i] = nil
            workerTimesLeft[i] = nil
        }
    }
    answer += completed.sorted()
}
print(answer)
print(time)
