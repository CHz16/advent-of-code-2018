#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "12.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

var rules: [String: String] = [:]
var initialState: String = ""
input.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: " ")
    if components[0] == "initial" {
        initialState = components[2]
    } else {
        rules[components[0]] = components[2]
    }
}

var leftIndex = 0
var state = initialState
for _ in 0..<20 {
    print(state)
    var newState = ""

    let paddedState = "......" + state + "......"
    var windowStart = paddedState.startIndex, windowEnd = paddedState.index(windowStart, offsetBy: 5)
    while true {
        newState += rules[String(paddedState[windowStart..<windowEnd])] ?? "."
        if windowEnd == paddedState.endIndex {
            break
        }

        windowStart = paddedState.index(after: windowStart)
        windowEnd = paddedState.index(after: windowEnd)
    }

    leftIndex -= 4
    while newState.first! == "." {
        leftIndex += 1
        newState.removeFirst()
    }
    while newState.last! == "." {
        newState.removeLast()
    }

    state = newState
}
print(state)
print(state.enumerated().reduce(0) { $0 + ($1.element == "#" ? ($1.offset + leftIndex) : 0) })


// Part 2

print("--------------")

var currentStep = 20
var lastState: String, lastScore: Int
repeat {
    lastState = state
    lastScore = lastState.enumerated().reduce(0) { $0 + ($1.element == "#" ? ($1.offset + leftIndex) : 0) }

    var newState = ""

    let paddedState = "......" + state + "......"
    var windowStart = paddedState.startIndex, windowEnd = paddedState.index(windowStart, offsetBy: 5)
    while true {
        newState += rules[String(paddedState[windowStart..<windowEnd])] ?? "."
        if windowEnd == paddedState.endIndex {
            break
        }

        windowStart = paddedState.index(after: windowStart)
        windowEnd = paddedState.index(after: windowEnd)
    }

    leftIndex -= 4
    while newState.first! == "." {
        leftIndex += 1
        newState.removeFirst()
    }
    while newState.last! == "." {
        newState.removeLast()
    }

    state = newState
    currentStep += 1
} while lastState != state

let score = state.enumerated().reduce(0) { $0 + ($1.element == "#" ? ($1.offset + leftIndex) : 0) }
print(state)
print("equilibrium reached at", currentStep)
print("current score", score, "| delta:", score - lastScore)
print(score + (score - lastScore) * (50000000000 - currentStep))
