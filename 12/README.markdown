Day 12: Subterranean Sustainability
===================================

https://adventofcode.com/2018/day/12

Fairly simple cellular automata puzzle. I spent a little while on part 1 trying to be clever by implementing it with a `map` before deciding naw, let's just concatenate strings, this isn't going to get too large probably (and it didn't). The "clever" bit of this implementation I guess is that it only keeps track of as much of the string as it needs to, by trimming dead cells on the ends and keep track of what the index on the number line that our string slice actually starts at, which accidentally came in handy in part 2.

Part 2, the insight is to run it enough times before you notice a pattern. Here, on my input and the test input, eventually the string reaches a point where the configuration of plants is exactly the same in every step, just translated. From that point, the score will increase or decrease by a constant amount every step, so all you have to do is take the score of that step and do some simple math to find the final score.

This turned out to be extremely easy to detect in my code with the way I was storing the state, just a string comparison with the last state is necessary! Once my code finds that, it just calculates the score difference from the last state and finishes up.

* Part 1: 329th place (24:19)
* Part 2: 231st place (37:47)
